#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include"queue.h"
#include"optimalFunctions.h"
#include"LRU.h"

#define MAX_PAGES 4095

void processArgs(int numArgs, char *argv[]);
void readFromFile(char *fileName, int *input);
void printUsage();
void simulateFIFO(int pageRequests[], int memorySize, FILE *fp);
void simulateOptimal(int pageRequests[], int memorySize, FILE *fp);
void simulateLRU(int pageRequests[], int memorySize, FILE *fp);

int main(int argc, char *argv[]) {

   int pageRequests[NUM_PAGE_REQUESTS];
   FILE *out;
   
   //process the arguments
   processArgs(argc, argv);
   readFromFile(argv[2], pageRequests);

   //open the output file for reading
   out = fopen(argv[3], "a");
   if(out == NULL) {
      printf("Could not open %s. Aborting.\n", argv[3]);
      exit(EXIT_FAILURE);
   }
   
   fprintf(out, "=================================================================================\n");
   fprintf(out, "\tPage Replacement Algorithm Simulation (frame size = %s)\n", argv[1]); 
   fprintf(out, "=================================================================================\n");
   fprintf(out, "\t\t\tPage fault rates\n");
   fprintf(out, "%-10s%-10s\t%-10s%-10s%-10s%-10s%-10s\n",
      "Algorithm","Total Page Faults","2000","4000","6000","8000","10000");
   fprintf(out, "---------------------------------------------------------------------------------\n");

   simulateFIFO(pageRequests,atoi(argv[1]), out);
   simulateOptimal(pageRequests,atoi(argv[1]), out);
   simulateLRU(pageRequests,atoi(argv[1]), out);

   fclose(out);
   return 0;
}

void simulateLRU(int pageRequests[], int memorySize, FILE *fp) {

   int LRUTable[MAX_PAGE_NUM];
   Queue frameTable;
   int i, pageFaults = 0;

   for(i = 0; i < MAX_PAGE_NUM; i++) {
      LRUTable[i] = -1;
   }

   //init the data structure
   frameTable.capacity = memorySize;
   frameTable.head = frameTable.tail = -1; //won't be utilizing these members
   frameTable.currentSize = 0;
   frameTable.data = (Page*) malloc(sizeof(Page) * frameTable.capacity);

   //array for stats
   double stats[5];
   int statCntr = 0;
   fprintf(fp, "%-10s\t", "LRU");


   for(i = 0; i < NUM_PAGE_REQUESTS; i++) {
      
      //record the used "time" of this page reference
      LRUTable[pageRequests[i]] = i;

      if(!arrayContains(frameTable, pageRequests[i])) {
         pageFaults++;
         replaceLRU(&frameTable, pageRequests[i], LRUTable);
      } 
      if(i > 0 && i % 2000 == 0) {
         stats[statCntr] = (double)(pageFaults) / i;
         statCntr++;
      } 
   }
   stats[statCntr] = (double)(pageFaults) / i;

   fprintf(fp, "%-10d\t", pageFaults);

   for(i = 0; i < 5; i++) {
      fprintf(fp, "%-10.3f", stats[i]);
   }
   fprintf(fp, "\n"); 

   free(frameTable.data);
}


void simulateOptimal(int pageRequests[], int memorySize, FILE *fp) {

   Queue frameTable;
   int i, pageFaults = 0;

   //init the data structure
   frameTable.capacity = memorySize;
   frameTable.head = frameTable.tail = -1; //won't be utilizing these members
   frameTable.currentSize = 0;
   frameTable.data = (Page*) malloc(sizeof(Page) * frameTable.capacity);

   //array for stats
   double stats[5];
   int statCntr = 0;
   fprintf(fp, "%-10s\t", "Optimal");



   //simulate optimal page replacement
   for(i = 0; i < NUM_PAGE_REQUESTS; i++) {

      if(!arrayContains(frameTable, pageRequests[i])) {
         lookAheadAndReplace(&frameTable,pageRequests, i);
         pageFaults++;
      }

      if(i > 0 && i % 2000 == 0 ) {
         stats[statCntr] = (double)(pageFaults) / i;
         statCntr++;
      }
   }

   stats[statCntr] = (double)(pageFaults) / i;

   fprintf(fp, "%-10d\t", pageFaults);

   for(i = 0; i < 5; i++) {
      fprintf(fp, "%-10.3f", stats[i]);
   }
   fprintf(fp, "\n"); 

   free(frameTable.data);
}

void simulateFIFO(int pageRequests[], int memorySize, FILE *fp) {

   Queue frameTable;
   int i, pageFaults = 0;

   //init the queue data structure
   frameTable.capacity = memorySize;
   frameTable.head = 0;
   frameTable.tail = -1;
   frameTable.currentSize = 0;
   frameTable.data = (Page*) malloc(sizeof(Page) * frameTable.capacity);
  
   //array for stats
   double stats[5];
   int statCntr = 0;
   fprintf(fp, "%-10s\t", "FIFO");

   //simulate FIFO
   for(i = 0; i < NUM_PAGE_REQUESTS; i++) {
      
      if(!contains(frameTable, pageRequests[i])) {
         pageFaults++; 
         queuePage(&frameTable, pageRequests[i]);
      }
      
      if(i > 0 && i % 2000 == 0 ) {
         stats[statCntr] = (double)(pageFaults) / i;
         statCntr++;
      } 
   }
   stats[statCntr] = (double)(pageFaults) / i;

   fprintf(fp, "%-10d\t", pageFaults);

   for(i = 0; i < 5; i++) {
      fprintf(fp, "%-10.3f", stats[i]);
   }
   fprintf(fp, "\n"); 
   free(frameTable.data);

}

void printUsage() {

   printf("Usage: memsim [frame size] [input file] [output file].\n");
   exit(EXIT_FAILURE);
}


void processArgs(int numArgs, char *argv[]) {
   
   if(numArgs < 4) {
      printf("Not enough arguments.\n");
      printUsage();
   } 
}

void readFromFile(char *fileName, int *input) {

   int request, i;
   FILE *fp = fopen(fileName, "r");    
   if(fp == NULL) {

      printf("Could not open file %s. Exiting.\n", fileName);
      exit(EXIT_FAILURE);
   }

  for(i = 0; i < NUM_PAGE_REQUESTS; i++) { 
      fscanf(fp, "%d", &request);
      input[i] = request; 
   }

   fclose(fp);
}
