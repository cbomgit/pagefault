#ifndef _PAGE
#define _PAGE

typedef struct Page {

   int value;
   int lastUsed;
} Page;

#endif

#ifndef _QUEUE
#define _QUEUE

typedef struct Queue {

   int head;
   int tail;
   int capacity;
   int currentSize;
   Page *data;
} Queue;
#endif

#ifndef NUM_PAGE_REQUESTS
#define NUM_PAGE_REQUESTS 10000
#endif

#ifndef MAX_PAGE_NUM
#define MAX_PAGE_NUM 4096
#endif
