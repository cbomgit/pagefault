#include "optimalFunctions.h"
#include <stdio.h>

void lookAheadAndReplace(Queue *frameTable, int pageRequests[], int currentRequest) {

   int longestNotUsedRequests = 0, ndx, pageIndex = currentRequest;
   int longestNotUsedNdx = 0;

   if(frameTable->currentSize == frameTable->capacity) {

      for(ndx = 0; ndx < frameTable->currentSize; ndx++) {

         pageIndex = currentRequest;
         while(pageIndex < NUM_PAGE_REQUESTS && 
            frameTable->data[ndx].value != pageRequests[pageIndex]) {
            pageIndex++;
         }
         if((pageIndex - currentRequest) > longestNotUsedRequests) {
            longestNotUsedRequests = pageIndex - currentRequest;
            longestNotUsedNdx = ndx;
         }
      }
      frameTable->data[longestNotUsedNdx].value = pageRequests[currentRequest];
         
   } else {

      frameTable->data[frameTable->currentSize].value = pageRequests[currentRequest];
      frameTable->currentSize++;
   }
   
   
}


//simple linear search to check if a page is contained in a frame table
//that uses the optimal page replacement algorithm
int arrayContains(Queue frameTable, int pageNum) {

   int i;
   for(i = 0; i < frameTable.currentSize; i++) {
      if(frameTable.data[i].value == pageNum) {
         return 1;
      }
   }
   return 0;
}
