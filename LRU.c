#include"LRU.h"
#include"types.h"
#include<stdio.h>

void replaceLRU(Queue *frameTable, int pageNum, int LRUTable[]) {

   int i = 0;
   int lru = frameTable->data[0].value;
   int lruIndex = 0;

   if(frameTable->currentSize == frameTable->capacity) {
      //find least recently used page reference
      for(i = 1; i < frameTable->currentSize; i++) {
         if(LRUTable[frameTable->data[i].value] < LRUTable[lru]) {
            lru = frameTable->data[i].value;
            lruIndex = i;
         }
      }
      frameTable->data[lruIndex].value = pageNum;

   } else {
      frameTable->data[frameTable->currentSize].value = pageNum;
      frameTable->currentSize++;
   }
}

      
