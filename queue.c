#include"queue.h"
#include<stdlib.h>
#include<stdio.h>

//void queuePage(int *frameTable, int *head, int *tail, int pageNum, int *currentSize, int capacity) {
void queuePage(Queue *frameTable, int pageNum) {
   //if maximum number of frames have been filled, then 
   //remove the oldest frame from the queue and add the newest frame 
   //to the end of the queue
   if(frameTable->currentSize == frameTable->capacity) {
      //dequeue the oldest frame and 
      dequeuePage(frameTable);
   }
   frameTable->tail = increment(frameTable->tail, frameTable->capacity);
   frameTable->data[(frameTable->tail)].value = pageNum;
   frameTable->currentSize++;
}

//void dequeuePage(int *frameTable, int *head, int *currentSize, int capacity) {
void dequeuePage(Queue *frameTable) {
   
   //fail if the current Queue is empty
   if(frameTable->currentSize > 0) {
      frameTable->currentSize--;
      frameTable->head = increment(frameTable->head, frameTable->capacity);
   }
}

int increment(int ndx, int capacity) {

   if(++ndx == capacity) {
      ndx = 0;
   }
   return ndx;
}

//int contains(int *frameTable, int head, int tail, int currentSize, int capacity, int page) {
int contains(Queue frameTable, int page) {
   if(frameTable.tail == -1 && frameTable.currentSize == 0) {
      return 0;
   }

   int i = frameTable.head;

   while(i != frameTable.tail) {
      if(frameTable.data[i].value == page) {
         return 1;
      }

      if(++i == frameTable.capacity) {
         i = 0;
      }
   }
   
   if(frameTable.data[frameTable.tail].value == page) {
      return 1;
   }

   return 0;
}
