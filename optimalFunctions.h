#ifndef _OPTIMAL
#define _OPTIMAL

#include "types.h"
#include "queue.h"

void lookAheadAndReplace(Queue *frameTable, int pageRequests[], int currentRequest);


int arrayContains(Queue frameTable, int pageNum);

#endif
