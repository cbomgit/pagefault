Christian Boman
Operating Systems Lab 3
Page Fault simulation


This program runs a simulation of 3 page replacement algorithms - 
FIFO, Optimal, and LRU. At run time, the number of frames in 
physical memory, a path to an input file, and a path to an output
file are passed to the program as arguments. The simulation runs through
each algorithm and calculates the page faults rates at 2000, 4000, 6000, and
10000 page requests. It also write the total # of page faults to a file.

$PATH_TO_EXECUTABLE/memsim [Number of frames in memory] [Path to input file]
[path to output file]

NOTE: The program appends the results to the specified file so any data
previously written to it will remain intact.

To build the program run "make" and to clean the program, run "make clean".
