/*This header file defines functions that implement queuing operations. The
  queue will hold a process struct defining a process. This definition is in 
  process.h. A queue is defined as an array with max size MAX_QUEUE_LENGTH. No
  resizing operation is supported. The functions below require that the caller
  maintain a pointer to the front and back of the queue as well as the queue's
  current size.
  
*/
#ifndef _QUEUE_FUNCTIONS
#define _QUEUE_FUNCTIONS

#include"types.h"
#define MAX_QUEUE_LENGTH 256

/*Adds an item to the end of the queue. If we have reached max capacity, then
  the program is abored.
*/

void queuePage(Queue *frameTable, int pageNum);
/*Removes and retrieves the item at the front of the queue. If the queue is
  empty, then a process with pid -1 is returned. Caller should check against
  the returned process pid.
*/
void dequeuePage(Queue *frameTable);

/*Internal function that adjusts the pointer to the head or tail of the queue.
  The pointer is implemented as an array index. If ndx reaches MAX_QUEUE_LENGTH
  it is set to 0.
*/
int increment(int ndx, int capacity);


int contains(Queue frameTable, int page);

#endif
